As you probably have heard - there has been a huge security breach reported by Equifax credit reporting agency. Hackers may have accessed and retrieved credit history of all individuals in their database. Which likely means pretty much everyone in the US has had their personal information, SSNs, accounts, addresses etc fall into the hands of individuals who may use it to establish fraudulent accounts, perpetuate identify theft, etc.

My family has reached out to me to get my opinion on what they should do to minimize the chances of getting affected by this, and personally I have been pretty concerned about it on my own as well - I have had a couple of run-ins with credit card fraud which made me somewhat sensitive to things like that. 

So I've researched the subject and came up with the following recipe for action that i thought may be valuable to others. Please share if you'd like.

1. Sign up to Credit Karma www.creditkarma.com . This service performs daily queries of your credit history from Equifax and TransUnion and allows you to keep an eye on it. It will also notify you if significant changes are observed in your credit history. If a new account is opened - you will know. It will also allow you to review your credit history already on file for any currently opened accounts that you don't recognize to make sure there're no signs of fraudulent activity. The service is free and reputable. They make money by offering you credit cards and various financial products, such as refinancing etc. There's also a very convenient phone app. 

If you don't wish to sign up for the service - you may request 1 free credit report per year at www.annualcreditreport.com as an alternative, however if you'd like to review your credit history more frequently than once a year, you will need to pay.

Reminder: please use unique and complex passwords when signing up with online services.

2. Following the breach, Equifax is offering a free credit monitoring service, along with Identity Theft insurance for a year to all people who have been affected. Go to www.equifaxsecurity2017.com , "Potential Impact" tab - and sign up. It appears that they have a large amount of requests and once you sign up - you may need to wait for a couple of days for them to process your information and send you the confirmation.

3. You may want to also place a credit freeze on your credit reporting accounts. When freeze is in place - anyone attempting to pull your credit information will get declined, potentially making it much more complicated to open new fraudulent accounts. However, if you need to open a new credit account yourself - you can temporarily or permanently lift the freeze with a pin number that you are given (don't lose it). If you'd like to do that, go to www.freeze.equifax.com . In the past I've done this and recall that there used to be a fee associated with this service, but it appears to be free of charge now - perhaps this is also due to the Equifax trying to make up for the breach. When credit freeze is placed - by law they are obligated to notify other credit agencies, so you don't need to perform this task more than once.

Keep in mind that if you intend to sign up with Credit Karma - you will have to do it first, before the freeze is in-place. Credit Karma continues to work if you place the freeze after you set it up. 

4. As an alternative to credit freeze, you can also consider placing a fraud alert. Fraud alert is a temporary red flag that is associated with your credit account that notifies credit providers to go through additional security checks before opening new accounts. Go to www.alerts.equifax.com to set it up. Just like credit freeze - you only need to do it once, and Equifax will notify the other credit agencies.

5. Whatever steps you chose to take - consider repeating them for all adults in the household.

Hope this helps.

Vlad.